VERSION=v2.3.6

build:
	go mod tidy && go build ./...

gomod:
	go get chainmaker.org/chainmaker/common/v2@$(VERSION)
	go get chainmaker.org/chainmaker/protocol/v2@v2.3.7
	go get chainmaker.org/chainmaker/libp2p-pubsub@v1.1.5
	go get chainmaker.org/chainmaker/net-common@v1.2.6
	go get chainmaker.org/chainmaker/logger/v2@v2.3.4
	go mod tidy
