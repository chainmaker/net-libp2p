package libp2pnet

import (
	"chainmaker.org/chainmaker/common/v2/random/uuid"
	api "chainmaker.org/chainmaker/protocol/v2"
	"encoding/json"
	"errors"
	"github.com/libp2p/go-libp2p-core/peer"
	"sync"
)

const (
	TOPIC_MSG_ROUTER = "msgRouter"
	ROUTER_COUNT     = 1
	ROUTER_MAX_STEP  = 5
)

type RouteMsg struct {
	From      peer.ID
	Middleman map[string]struct{}
	To        peer.ID
	MsgData   []byte
	MsgFlag   string
	MsgId     string
}

// newRouterController
// @Description:
// @param log
// @param ln
// @return ${return_type}
func newRouterController(log api.Logger, ln *LibP2pNet) *RouterController {
	return &RouterController{log: log, ln: ln}
}

// RouterController
// @Description:
type RouterController struct {
	ln        *LibP2pNet
	msgFilter sync.Map
	msgArr    []string
	log       api.Logger
}

// isRepeat
// @Description:
// @receiver r
// @param msgId
// @return ${return_type}
func (r *RouterController) isRepeat(msgId string) bool {
	return false
	//_, loaded := r.msgFilter.LoadOrStore(msgId, struct{}{})
	//if loaded {
	//	r.log.Debugf("isRepeat msgId(%v)", msgId)
	//	return true
	//}
	//return false
}

// startMsgRouter
// @Description:
// @receiver r
// @param chainId
// @return ${return_type}
func (r *RouterController) startMsgRouter(chainId string) {
	ln := r.ln
	log := r.log
	err := ln.DirectMsgHandle(chainId, TOPIC_MSG_ROUTER, func(sender string, msgData []byte) error {
		localPeerId := ln.libP2pHost.host.ID().Pretty()
		msg := &RouteMsg{Middleman: map[string]struct{}{}}
		err := json.Unmarshal(msgData, msg)
		if err != nil {
			log.Error("json err:", err)
			return nil
		}
		if localPeerId == msg.To.Pretty() {

			if ln.router.isRepeat(msg.MsgId) {
				return nil
			}
			log.Debugf("handler %v", string(msgData))
			handler := ln.messageHandlerDistributor.handler(chainId, msg.MsgFlag)
			if handler == nil {
				log.Warnf("[Net] handler not registered. drop message. (chainId:%s, flag:%s)", chainId, msg.MsgFlag)
				return nil
			}
			readMsgCallHandler(msg.From.Pretty(), msg.MsgData, handler, log)
			return nil
		}

		err = ln.router.msgRouter(chainId, msg)
		if err != nil {
			log.Debug("msg Router err:", err)
			return nil
		}
		return nil
	})
	if err != nil {
		log.Error("startMsgRouter chain(%v)err(%v)", chainId, err)
		return
	}

}

// stopMsgRouter
// @Description:
// @receiver r
// @param chainId
// @return ${return_type}
func (r *RouterController) stopMsgRouter(chainId string) {
	_ = r.ln.CancelDirectMsgHandle(chainId, TOPIC_MSG_ROUTER)
}

// sendByMsgRouter
// @Description:
// @receiver r
// @param chainId
// @param pid
// @param msgFlag
// @param data
// @return ${return_type}
func (r *RouterController) sendByMsgRouter(chainId string, pid peer.ID, msgFlag string, data []byte) error {
	msg := &RouteMsg{
		From:      r.ln.libP2pHost.host.ID(),
		To:        pid,
		MsgFlag:   msgFlag,
		MsgData:   data,
		MsgId:     uuid.GetUUID(),
		Middleman: map[string]struct{}{},
	}
	return r.msgRouter(chainId, msg)
}

// msgRouter
// @Description:
// @receiver r
// @param chainId
// @param msg
// @return ${return_type}
func (r *RouterController) msgRouter(chainId string, msg *RouteMsg) error {
	if len(msg.Middleman) > ROUTER_MAX_STEP {
		r.log.Warn("step(%v)>ROUTER_MAX_STEP(%v) msgId(%v)", len(msg.Middleman), ROUTER_MAX_STEP, msg.MsgId)
		return nil
	}
	localPeerId := r.ln.libP2pHost.host.ID().Pretty()
	msg.Middleman[localPeerId] = struct{}{}
	raw, err := json.Marshal(msg)
	if err != nil {
		r.log.Error("json err:", err)
		return err
	}
	if conn := r.ln.libP2pHost.connManager.GetConn(msg.To); conn != nil {
		return r.ln.SendMsg(chainId, msg.To.Pretty(), TOPIC_MSG_ROUTER, raw)
	}
	i := 0
	successCount := 0
	err = errors.New(" no conn(" + msg.To.Pretty() + ") to use")
	for _, v := range r.ln.libP2pHost.connManager.highLevelConn {
		if _, ok := msg.Middleman[v.pid.Pretty()]; ok {
			continue
		}
		err = r.ln.SendMsg(chainId, v.pid.Pretty(), TOPIC_MSG_ROUTER, raw)
		if err != nil {
			continue
		}
		successCount++
		i++
		if i >= ROUTER_COUNT {
			break
		}
	}
	if successCount > 0 {
		return nil
	}
	return err
}
