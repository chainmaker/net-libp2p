package libp2pnet

import (
	"net/http"
	"regexp"
	"testing"
)

func TestGetSendHttpTunnelTargetAddress(t *testing.T) {
	pid1 := "QmeyNRs2DwWjcHTpcVHoUSaDAAif4VQZ2wQDQNDP33gH"
	pid2 := "QmXf6mnQDBR9aHauRmViKzSuZgpumkn7x6rNxw1oqqRr45"
	pid3 := "QmRRWXJpAVdhFsFtd9ah5F4LDQWFFBDVKpECAF8hssqj6H"
	address1 := "http://example.org1"
	address2 := "127.0.0.1:8000"
	address3 := "example.org1:8000"
	//test1
	prepare := &LibP2pNetPrepare{
		regIP: regexp.MustCompile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}"),
	}
	nodeId, address := prepare.getSendHttpTunnelTargetAddress(pid1 + "/" + address1)
	if pid1 != nodeId {
		t.Error(pid1)
	}
	if address1 != address {
		t.Error(address1)
	}
	//test2
	nodeId, address = prepare.getSendHttpTunnelTargetAddress(pid2 + "/" + address2)
	if pid2 != nodeId {
		t.Error(pid2)
	}
	if "//"+address2 != address {
		t.Error(address2)
	}
	if _, err := http.NewRequest("CONNECT", address, nil); err != nil {
		t.Error(err)
	}
	//test3
	nodeId, address = prepare.getSendHttpTunnelTargetAddress(pid3 + "/" + address3)
	if pid3 != nodeId {
		t.Error(pid3)
	}
	if address3 != address {
		t.Error(address3)
	}
}
