package libp2pnet

import (
	"encoding/json"
	"time"
)

const TOPIC_CERTID = "certID"

type certId struct {
	CertId string
	PeerId string
}

func (ln *LibP2pNet) checkCertId(chainId string) {
	err := ln.SubscribeWithChainId(chainId, TOPIC_CERTID, func(publisherPeerId string, msgData []byte) error {
		msg := &certId{}
		err := json.Unmarshal(msgData, msg)
		if err != nil {
			ln.log.Error("json err:", err)
			return nil
		}
		_, err = ln.libP2pHost.certPeerIdMapper.FindPeerIdByCertId(msg.CertId)
		if err != nil {
			ln.log.Info("certPeerIdMapper.Add:", msg.PeerId)
			ln.libP2pHost.certPeerIdMapper.Add(msg.CertId, msg.PeerId)
		}
		return nil
	})
	if err != nil {
		ln.log.Error("SubscribeWithChainId err:", err)
		return
	}
	ticker := time.NewTicker(refreshCertIdTickerTime)

	for {
		select {
		case <-ln.ctx.Done():
			ln.log.Info("checkCertId return")
			return
		case <-ticker.C:
			certIdList := make([]*certId, 0)
			tmpCertIdList := ln.libP2pHost.certPeerIdMapper.GetAll()
			if len(tmpCertIdList) == 0 {
				continue
			}
			if len(tmpCertIdList)%2 != 0 {
				ln.log.Error("certPeerIdMapper return err,", tmpCertIdList)
				continue
			}
			for i := 0; i < len(tmpCertIdList); i += 2 {
				msg := &certId{
					CertId: tmpCertIdList[i],
					PeerId: tmpCertIdList[i+1],
				}
				certIdList = append(certIdList, msg)
			}

			for _, msg := range certIdList {
				raw, err := json.Marshal(msg)
				if err != nil {
					ln.log.Error("json err:", err)
					continue
				}
				err = ln.BroadcastWithChainId(chainId, TOPIC_CERTID, raw)
				if err != nil {
					ln.log.Warn("BroadcastWithChainId err:", err)
					if err.Error() == ErrorPubSubNotExist.Error() {
						ln.log.Info("checkCertId return")
						return
					}
				}
			}

		}
	}

}
